module.exports = {
    css: {
        loaderOptions: {
        sass: {
            data: `
                @import "@/assets/css/variables.scss";
                @import "@/assets/css/mixin.scss";
                `
            }
        }
    },
    devServer: {
        proxy: 'http://localhost:8081'
      }
};