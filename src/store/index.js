import Vue from 'vue';
import Vuex from 'vuex';

import keys from '../assets/json/exchanges.json'

var ccxt = require ('ccxt');
const proxy = 'http://localhost:8080/';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        keys,
        balance: {},
        countExchanges: 0,
        address: {
            addr: '',
            status: '',
            message: ''
        },
        withdraw: {
            message: '',
            status: '',
            result: ''
        }
    },
    getters: {
        getExchangeRequests(state) {
            if(Object.keys(state.keys).length === state.countExchanges) {
                return true
            }
        },
        getExchangesItems(state) {
            let result = {};
            //Идем по массиву всех бирж
            for(var exchange in state.balance) {
                let exchange_main = state.balance[exchange],
                    balance_main = exchange_main.balance;

                // Проверяем что поле баланс уже есть у главной биржи
                if(balance_main) {
                    // Идем по списку монет в главной бирже
                    for(let coin_main in balance_main) {
                        // Если главная монеты больше чем 1
                        if(balance_main[coin_main] >=1) {
                            
                            // Сохраняем имя ненулевой галвной монеты и биржи в которой она нашлась
                            let coin_current = coin_main;
                            let exchange_current = exchange_main;

                            // Идем по массиву бирж за исключением главной биржи

                            for(var target in state.balance) {
                                let exchange_target = state.balance[target],
                                    balance_target = exchange_target.balance;

                                if(exchange_target.id != exchange_current.id) {

                                    // Идем по списку монет во второй бирже
                                    for(let coin_target in balance_target) {
                                        // Если вторая монета = ненулевой главная монета
                                        if(coin_target === coin_current) {

                                            // Если текущей биржи еще нет в общем списке, добавляем ее и создаем поле coins для монет
                                            if(!result[exchange_current.id]) {
                                                result[exchange_current.id] = {
                                                    name: exchange_current.name,
                                                    coins: {}
                                                }
                                            }
                                            
                                            //  Если такой монеты еще нет в списке для текущей биржи, создаем ее и записываем имя, баланс и массив для записи бирж
                                            if(!result[exchange_current.id].coins[coin_current]) {
                                                result[exchange_current.id].coins[coin_current] = {
                                                    id: coin_current,
                                                    balance: exchange_current.balance[coin_current],
                                                    exchanges: []
                                                }
                                            }

                                            // Записываем совпавшую биржу в общий список
                                            result[exchange_current.id].coins[coin_current].exchanges.push({id: exchange_target.id, name: exchange_target.name})

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            // ПРоверяем что обхект с результатами не пустой
            if(Object.entries(result).length != 0 && result.constructor === Object) {
                return result
            }
            
        },
        getAddress(state) {
            return state.address
        },
        getWithdraw(state) {
            return state.withdraw
        }
    },
    mutations: {
        countExchanges(state) {
            state.countExchanges = state.countExchanges + 1
        },
        setBalance(state, data) {
            Vue.set(state.balance, data.id, data);
        },
        setAddress(state, val) {
            state.address = val;
        },
        setWithdraw(state, data) {
            state.withdraw = data;
        },
        clearAddrAndWithdraw(state) {
            state.address = {
                addr: '',
                status: '',
                message: ''
            },
            state.withdraw = {
                message: '',
                status: '',
                result: ''
            }
        }
    },
    actions: {
        async getExchangesList(store) {
            // Идем по массиву бирж

            for(let key in keys) {
                let exchange = new ccxt[key]({
                    proxy,
                    'apiKey': keys[key].apiKey,
                    'secret': keys[key].secret,
                    'timeout': 10000000
                });

                let balance = await exchange.fetchBalance();
                let name = exchange.name;
                let id = exchange.id;

                store.commit('countExchanges')

                store.commit('setBalance',{
                    balance: balance.total,
                    name,
                    id
                })
            }
        },
        async requestAddress(store, info) {
            /***
             * info: {
             *  coin,
             *  exchange_id
             * }
             */
            store.commit('setAddress', {addr: 'pending...', status: 'pending', message: ''})
            let key = store.state.keys[info.exchange];
            let exchange = new ccxt[info.exchange]({
                proxy,
                'apiKey': key.apiKey,
                'secret': key.secret,
                'timeout': 10000000
            });
            try {
                let addr = await exchange.fetchDepositAddress(info.coin);
                if(addr.address) {
                    store.commit('setAddress', {addr: addr.address, status: 'success', message: ''})
                } else {
                    store.commit('setAddress', {addr: '', status: 'error', message: addr})
                    console.log(addr)
                }
            } catch(e) {
                store.commit('setAddress', {addr: '', status: 'error', message: e})
            }
        },
        async requestWithdraw(store, data) {
            /***
             * data: {
             *  exchange,
             *  coin,
             *  amount
             * }
             */
            
            store.commit('setWithdraw', {status: 'pending'})
            let key = store.state.keys[data.exchange];
            let exchange = new ccxt[data.exchange]({
                proxy,
                'apiKey': key.apiKey,
                'secret': key.secret,
                'timeout': 10000000
            });
            try {
                // setTimeout(()=>{
                //     let result = {
                //         "info": {
                //             "status": "ok", 
                //             "data": 11561728
                //             }, 
                //         "id": 11561728
                //     }
                //     store.commit('setWithdraw', {result: 'ok', status: 'success', message: result})
                // },500)
                console.log(data.coin, data.amount, store.state.address.addr)
                let result = exchange.withdraw(data.coin, data.amount, store.state.address.addr, tag = undefined, params = {})
                if(result.info) {
                    store.commit('setWithdraw', {result: 'ok', status: 'success', message: result})
                } else {
                    store.commit('setWithdraw', {result: 'ok', status: 'error', message: result})
                }
            } catch(e) {
                store.commit('setWithdraw', {result: 'ok', status: 'error', message: e})
                console.log(e)
            }
        },
        clearAddrAndWithdraw(store) {
            store.commit('clearAddrAndWithdraw')
        }
    }
});